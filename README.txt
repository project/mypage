README.txt
----------
This module gives each user an opportunity to select some of the taxonomy terms available on the site, and have a custom "mypage" generated which lists only nodes from the selected terms, at URL ?q=mypage/view


Usage examples
--------------
For example, on a school site, if every class has its own taxonomy term, students don't want to see info about every class at the school, just the ones he/she is enrolled in.  Mypage allows the users to select the correct taxonomies.

This could also be used on a news site, where users only want to see news related to certain subjects.


Requirements
------------
This module requires version 4.5 of Drupal.  It was tested using MySQL (not tested against Postgres).  The original 4.4 version is available from http://drupal.org.  There are no new features in this version, it was upgraded for compatibility with Drupal 4.5 only.


Author
------
Andy Goldberg (drupaldev@andy2.com)


Installation
------------
1. Execute the mypage.mysql code on your MySQL DB.  You are adding two columns to the user table, so you could do it manually if you have access to the database.

2. Copy mypage.module to the Drupal modules/ folder.
   
3. Enable "mypage" in 'administer -> configuration -> modules'

4. Try it out by visiting 'my account -> build my page', and then 'view my page'

5. You can also recieve customized RSS feeds: if you are logged in, the URL "?q=mypage/feed" will give you your feed.  If not, "?q=mypage/feed/username" will work.  Note that you will need to adjust permissions to allow non-logged in users to view feeds (see the comments in the module code), and this may incur a security risk if your site content is private.

6. There are no administration pages for this module.  The only additional menu options are "view my page" and "build my page" (under "my account") for each user.


Extras
------
Having the "mypage" subscriptions adds a number of possibilities to your Drupal site.  I have utilized the mypage subscriptions within the BlogAPI module (so a user can post only to the categories he/she is subscribed to), and to the Archive module (so a user can look for posts only within his/her categories).  Please e-mail me if you'd like more info about these patches.
